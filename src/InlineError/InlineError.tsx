import React from 'react'
import classNames from 'classnames'

import './InlineError.scss'

export interface InlineErrorProps {
  children: React.ReactNode
  className?: string
}

export const InlineError = (props: InlineErrorProps) => (
  <div className={classNames('InlineError', props.className)}>
    {props.children}
  </div>
)
