import React from 'react'

import './LayoutWithBackground.scss'

interface LayoutWithBackgroundProps {
  children?: React.ReactNode
}

export const LayoutWithBackground = (props: LayoutWithBackgroundProps) => {
  return <div className='LayoutWithBackground'>{props.children}</div>
}
