import React from 'react'

import './LabeledValue.scss'

export interface LabeledValueProps {
  label?: string
  children?: React.ReactNode
}

export function LabeledValue(props: LabeledValueProps) {
  return (
    <div className='LabeledValue'>
      {props.label && <div className='LabeledValue-label'>{props.label}</div>}
      <div className='LabeledValue-value'>{props.children}</div>
    </div>
  )
}
