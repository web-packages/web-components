# web-components

Web-components is a library that contains shared components of roommate app.

## Installation
You first need to add components library repository as a dependency.

Inside your project go to `package.json` file and add the following line under `dependencies`:

```json
"web-components": "git+https://web-token:nAmxw2xqRsLsNZWWbU9y@gitlab.com/gentrithumolli/web-components.git#main"
```

Then install the package by running the following command:

```bash
npm install --save web-components
```

## Usage
Simple usage example with InlineSpinner component:

```javascript
import { InlineSpinner, InlineSpinnerProps } from 'web-components'

export const CustomInlineSpinner = (props:InlineSpinnerProps)=>{
	return <InlineSpinner {...props} type="grow" />
}
```

## Storybook
Roommate web-components library storybook provides you the neccessary documentation and usage examples for every component and it's variations.

To access storybook playground you have to go inside component library folder, open terminal and run the following command:

```bash
npm run storybook
```

