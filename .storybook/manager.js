import { addons } from '@storybook/addons'
import { create } from '@storybook/theming'

addons.setConfig({
  theme: create({
    base: 'light',
    brandTitle: 'RoomMate Components Library',
    brandUrl: 'https://www.roommate.no',
    brandImage:
      'https://www.roommate.no/wp-content/themes/roommate2017/img/roommate-logo.svg'
  })
})
