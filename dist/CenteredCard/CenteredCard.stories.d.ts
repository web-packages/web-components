/// <reference types="react" />
declare const _default: {
    title: string;
    component: (props: import("./CenteredCard").CenteredCardProps) => JSX.Element;
};
export default _default;
export declare const WithIcon: () => JSX.Element;
export declare const WithoutIcon: () => JSX.Element;
export declare const Interactive: any;
