import React from 'react';
import './CenteredCard.scss';
export interface CenteredCardProps {
    className?: string;
    children?: React.ReactNode;
    showIcon?: boolean;
}
export declare const CenteredCard: (props: CenteredCardProps) => JSX.Element;
