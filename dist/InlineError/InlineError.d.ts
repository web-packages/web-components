import React from 'react';
import './InlineError.scss';
export interface InlineErrorProps {
    children: React.ReactNode;
    className?: string;
}
export declare const InlineError: (props: InlineErrorProps) => JSX.Element;
