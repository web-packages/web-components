import React from 'react';
export interface ButtonProps {
    children: React.ReactNode;
}
export declare const Button: (props: ButtonProps) => JSX.Element;
