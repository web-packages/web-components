declare const _default: (props?: any) => Record<"search" | "searchIcon" | "inputRoot" | "inputInput" | "highlighted", string>;
export default _default;
