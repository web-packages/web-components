function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var cs = _interopDefault(require('classnames'));
var reactstrap = require('reactstrap');

var Button = function Button(props) {
  return React.createElement("button", {
    style: {
      color: 'red'
    }
  }, props.children);
};

var CenteredCard = function CenteredCard(props) {
  return React.createElement("div", {
    className: cs('CenteredCard', props.className)
  }, React.createElement("div", {
    className: 'container'
  }, React.createElement("div", {
    className: 'row'
  }, React.createElement("div", {
    className: 'col-lg-8 offset-lg-2 col-md-10 offset-md-1'
  }, React.createElement("div", {
    className: 'CenteredCard-wrapper mb-3'
  }, props.children, props.showIcon === false ? null : React.createElement("span", {
    className: 'CenteredCard-wrapper-icon'
  }))))));
};

var LayoutWithBackground = function LayoutWithBackground(props) {
  return React.createElement("div", {
    className: 'LayoutWithBackground'
  }, props.children);
};

function LabeledValue(props) {
  return React.createElement("div", {
    className: 'LabeledValue'
  }, props.label && React.createElement("div", {
    className: 'LabeledValue-label'
  }, props.label), React.createElement("div", {
    className: 'LabeledValue-value'
  }, props.children));
}

var InlineError = function InlineError(props) {
  return React.createElement("div", {
    className: cs('InlineError', props.className)
  }, props.children);
};

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function InlineSpinner(props) {
  return React.createElement("span", {
    className: cs('InlineSpinner', props.className)
  }, React.createElement(reactstrap.Spinner, {
    size: props.size || 'sm',
    type: props.type || 'border',
    className: cs('InlineSpinner-spinner', props.spinnerClassName)
  }), props.text && React.createElement("span", {
    className: cs('InlineSpinner-text', props.textClassName),
    style: _extends({}, props.textStyle)
  }, props.text));
}

exports.Button = Button;
exports.CenteredCard = CenteredCard;
exports.InlineError = InlineError;
exports.InlineSpinner = InlineSpinner;
exports.LabeledValue = LabeledValue;
exports.LayoutWithBackground = LayoutWithBackground;
//# sourceMappingURL=index.js.map
