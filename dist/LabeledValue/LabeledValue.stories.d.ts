/// <reference types="react" />
import { LabeledValue as LV } from './LabeledValue';
declare const _default: {
    title: string;
    component: typeof LV;
};
export default _default;
export declare const LabeledValue: () => JSX.Element;
export declare const LabeledValueGroup: () => JSX.Element;
export declare const WithControls: any;
