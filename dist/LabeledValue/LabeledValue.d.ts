import React from 'react';
import './LabeledValue.scss';
export interface LabeledValueProps {
    label?: string;
    children?: React.ReactNode;
}
export declare function LabeledValue(props: LabeledValueProps): JSX.Element;
