import React from 'react';
import cs from 'classnames';
import { Spinner } from 'reactstrap';

const Button = props => {
  return React.createElement("button", {
    style: {
      color: 'red'
    }
  }, props.children);
};

const CenteredCard = props => React.createElement("div", {
  className: cs('CenteredCard', props.className)
}, React.createElement("div", {
  className: 'container'
}, React.createElement("div", {
  className: 'row'
}, React.createElement("div", {
  className: 'col-lg-8 offset-lg-2 col-md-10 offset-md-1'
}, React.createElement("div", {
  className: 'CenteredCard-wrapper mb-3'
}, props.children, props.showIcon === false ? null : React.createElement("span", {
  className: 'CenteredCard-wrapper-icon'
}))))));

const LayoutWithBackground = props => {
  return React.createElement("div", {
    className: 'LayoutWithBackground'
  }, props.children);
};

function LabeledValue(props) {
  return React.createElement("div", {
    className: 'LabeledValue'
  }, props.label && React.createElement("div", {
    className: 'LabeledValue-label'
  }, props.label), React.createElement("div", {
    className: 'LabeledValue-value'
  }, props.children));
}

const InlineError = props => React.createElement("div", {
  className: cs('InlineError', props.className)
}, props.children);

function InlineSpinner(props) {
  return React.createElement("span", {
    className: cs('InlineSpinner', props.className)
  }, React.createElement(Spinner, {
    size: props.size || 'sm',
    type: props.type || 'border',
    className: cs('InlineSpinner-spinner', props.spinnerClassName)
  }), props.text && React.createElement("span", {
    className: cs('InlineSpinner-text', props.textClassName),
    style: { ...props.textStyle
    }
  }, props.text));
}

export { Button, CenteredCard, InlineError, InlineSpinner, LabeledValue, LayoutWithBackground };
//# sourceMappingURL=index.modern.js.map
