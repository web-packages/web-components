import React from 'react';
import './LayoutWithBackground.scss';
interface LayoutWithBackgroundProps {
    children?: React.ReactNode;
}
export declare const LayoutWithBackground: (props: LayoutWithBackgroundProps) => JSX.Element;
export {};
