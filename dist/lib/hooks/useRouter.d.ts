import { RouteComponentProps, StaticContext } from 'react-router';
export declare function useRouter<T = {}>(): RouteComponentProps<T, StaticContext, any>;
