import { CSSProperties } from 'react';
import './InlineSpinner.scss';
export interface InlineSpinnerProps {
    text?: string;
    size?: string;
    type?: 'grow' | 'border';
    textStyle?: CSSProperties;
    className?: string;
    spinnerClassName?: string;
    textClassName?: string;
}
export declare function InlineSpinner(props: InlineSpinnerProps): JSX.Element;
