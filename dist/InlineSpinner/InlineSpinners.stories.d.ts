/// <reference types="react" />
import { InlineSpinner } from './InlineSpinner';
declare const _default: {
    title: string;
    component: typeof InlineSpinner;
    argTypes: {
        className: {
            control: boolean;
        };
        spinnerClassName: {
            control: boolean;
        };
        textClassName: {
            control: boolean;
        };
    };
};
export default _default;
export declare const Default: () => JSX.Element;
export declare const DefaultWithText: () => JSX.Element;
export declare const Grow: () => JSX.Element;
export declare const GrowLarge: () => JSX.Element;
export declare const GrowWithText: () => JSX.Element;
export declare const Interactive: any;
